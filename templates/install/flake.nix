{
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-23.11";
    sehqlr-config.url = "path:./config";
  };
  outputs = { self, nixpkgs, sehqlr-config, ... }@inputs:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };
    in {
      nixosConfigurations."nixos" = nixpkgs.lib.nixosSystem {
        inherit system;
        modules = with sehqlr-config.nixosModules; [
          ./configuration.nix
          common
          ({ config, ... }: {
              config.home-manager.users.sehqlr.home.stateVersion = "23.11";
          })
          # desktop
          # web
          # workspace
          # personal
        ];
      };
    };
}
