{
  description = "Configuration flake for sehqlr and their machines";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-23.11";

    home-manager.url = "github:nix-community/home-manager/release-23.11";

    nur.url = "github:nix-community/NUR";
  };

  outputs = { self, nixpkgs, home-manager, nur, ... }:
    let
      system = "x86_64-linux";
      overlay = nur.overlay;
    in {
      templates = {
        install = {
          path = ./templates/install;
          description = "A flake to bootstrap a new computer install";
        };
        default = self.templates.install;
      };
      nixosModules = {
        common = { config, lib, pkgs, ... }: {
          imports = [ home-manager.nixosModules.home-manager ./modules/common ];
          config.nixpkgs.overlays = [ overlay ];
        };
        desktop = { config, lib, pkgs, ... }: {
          imports =
            [ home-manager.nixosModules.home-manager ./modules/desktop ];
        };
        web = { config, lib, pkgs, ... }: { imports = [ ./modules/web ]; };
        workspace = { config, lib, pkgs, ... }: {
          imports = [ home-manager.nixosModules.home-manager ];
          config = {
            home-manager.users.sehqlr = {
              home.packages = with pkgs; [ jetbrains.pycharm-community ];
            };
            virtualisation.virtualbox.guest.enable = true;
          };
        };
        personal = { config, lib, pkgs, ... }: {
          imports = [ home-manager.nixosModules.home-manager ];
          config = {
            programs.steam = {
              enable = true;
              remotePlay.openFirewall = true;
            };
            home-manager.users.sehqlr = {
              home.packages = with pkgs; [
                discord
                element-desktop
                lutris
                slack
                spotify
                zoom-us
              ];
            };
          };
        };
      };
    };
}
