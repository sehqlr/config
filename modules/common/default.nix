{ pkgs, ... }: {
  environment = {
    systemPackages = with pkgs; [ commonsCompress git zlib ];
    pathsToLink = [ "/share/zsh" ];
  };

  fonts.enableDefaultPackages = true;

  i18n.defaultLocale = "en_US.UTF-8";

  networking.networkmanager.enable = true;

  nix = {
    gc.automatic = true;
    package = pkgs.nixUnstable;
    settings = {
      auto-optimise-store = true;
      trusted-users = [ "root" "sehqlr" ];
    };
  };

  services.borgbackup.jobs.sehqlr = {
    paths = "/home/sehqlr";
    repo = "/root/backup";
    encryption = {
      mode = "repokey";
      passCommand = "cat /root/borgbackup_passphrase";
    };
    compression = "auto,lzma";
    startAt = "daily";
  };

  nixpkgs.config = import ../../cfgs/nixpkgs-config.nix;

  programs.zsh.enable = true;

  users.users.sehqlr = {
    description = "sehqlr";
    extraGroups = [ "wheel" "networkmanager" ];
    isNormalUser = true;
    shell = pkgs.zsh;
  };

  home-manager = {
    useGlobalPkgs = true;
    useUserPackages = true;
    users.sehqlr = {
      home = {
        file.".cabal/config".source = ../../cfgs/cabal-config;
        packages = with pkgs; [
          age
          aspell
          aspellDicts.en
          aspellDicts.en-computers
          cabal-install
          httpie
          nixfmt
          pandoc
          python3Packages.editorconfig
          ripgrep
          sops
          vale
        ];
        sessionVariables.EDITOR = "kak";
      };

      programs.bat.enable = true;

      programs.direnv = {
        enable = true;
        enableZshIntegration = true;
        config.global.strict_env = true;
      };

      programs.fzf = {
        enable = true;
        enableZshIntegration = true;
      };

      programs.git = {
        enable = true;
        aliases = {
          graph = "log --graph --oneline --decorate";
          staged = "diff --cached";
          wip =
            "for-each-ref --sort='authordate:iso8601' --format='%(color:green)%(authordate:relative)%09%(color:white)%(refname:short)' refs/heads";
        };
        extraConfig = { init.defaultBranch = "main"; };
        ignores = [ "result" ];
        lfs.enable = true;
        userEmail = "hey@samhatfield.me";
        userName = "sehqlr";
      };

      programs.go.enable = true;

      programs.jq.enable = true;

      programs.kakoune = {
        enable = true;
        config = {
          colorScheme = "solarized-dark";
          hooks = [
            {
              name = "WinCreate";
              option = "^[^*]+$";
              commands = ''
                editorconfig-load
              '';
            }
            {
              name = "WinSetOption";
              option = "filetype=markdown";
              commands = ''
                set-option buffer lintcmd 'vale'
                set-option buffer formatcmd 'pandoc -f markdown -t markdown-smart -s'
              '';
            }
            {
              name = "BufCreate";
              option = "^.*lhs$";
              commands = "set-option buffer filetype markdown";
            }
            {
              name = "InsertChar";
              option = "j";
              commands = ''
                try %{
                    exec -draft hH <a-k>jj<ret> d
                    exec <esc>
                }
              '';
            }
            {
              name = "BufCreate";
              option = "^.*nix$";
              commands = ''
                set-option buffer formatcmd 'nixfmt'
                set-option buffer lintcmd 'nix-linter'
              '';
            }
            {
              name = "WinSetOption";
              option = "filetype=(haskell|javascript|python)";
              commands = ''
                lsp-enable-window
              '';
            }
          ];
          keyMappings = [
            {
              docstring = "LSP mode";
              effect = ":enter-user-mode lsp<ret>";
              key = "l";
              mode = "user";
            }
            {
              docstring = "Select next snippet placeholder";
              effect =
                ":try lsp-snippets-select-next-placeholders catch %{ execute-keys -with-hooks  <lt>tab> }<ret>";
              key = "<tab>";
              mode = "insert";
            }
            {
              docstring = "LSP any symbol";
              effect = "<a-semicolon>lsp-object<ret>";
              key = "a";
              mode = "object";
            }
            {
              docstring = "LSP any symbol";
              effect = "<a-semicolon>lsp-object<ret>";
              key = "<a-a>";
              mode = "object";
            }
            {
              docstring = "LSP function or method";
              effect = "<a-semicolon>lsp-object Function Method<ret>";
              key = "e";
              mode = "object";
            }
            {
              docstring = "LSP class interface or struct";
              effect = "<a-semicolon>lsp-object Class Interface Struct<ret>";
              key = "k";
              mode = "object";
            }
            {
              docstring = "LSP errors and warnings";
              effect =
                "<a-semicolon>lsp-diagnostic-object --include-warnings<ret>";
              key = "d";
              mode = "object";
            }
            {
              docstring = "LSP errors";
              effect = "<a-semicolon>lsp-diagnostic-object<ret>";
              key = "D";
              mode = "object";
            }
          ];
          numberLines.enable = true;
          showMatching = true;
          showWhitespace.enable = true;
          ui.enableMouse = true;
          wrapLines = {
            enable = true;
            marker = "⏎";
            word = true;
          };
        };
        plugins = [ pkgs.kakounePlugins.kak-lsp ];
        extraConfig = ''
          eval %sh{kak-lsp --kakoune  -s $kak_session}
        '';
      };

      programs.ssh = {
        enable = true;
        matchBlocks = {
          "git.bytes.zone" = {
            port = 2222;
            user = "git";
            identityFile = "~/.ssh/id_rsa";
            extraOptions = {
              HostKeyAlgorithms = "+ssh-rsa";
              PubkeyAcceptedKeyTypes = "+ssh-rsa";
            };
          };
          "github.com" = {
            user = "git";
            identityFile = "~/.ssh/id_rsa";
          };
          "gitlab.com" = {
            user = "git";
            identityFile = "~/.ssh/id_rsa";
          };
          "samhatfield.me" = {
            user = "root";
            identityFile = "~/.ssh/id_rsa";
          };
          "sknowball-beta" = {
            user = "root";
            identityFile = "~/.ssh/id_rsa";
          };
        };
      };

      programs.starship = {
        enable = true;
        enableZshIntegration = true;
        settings = {
          add_newline = false;
          character = {
            success_symbol = "[λ](bold·green)";
            error_symbol = "[λ](bold·red)";
          };
        };
      };

      programs.tmux = {
        enable = true;
        clock24 = true;
        extraConfig = ''
          set -g mouse on
        '';
        plugins = with pkgs; [{
          plugin = tmuxPlugins.tmux-colors-solarized;
          extraConfig = ''
            set -g @colors-solarized 'dark'
            set -g status-right ""
          '';
        }];
        terminal = "tmux-256color";
        shortcut = "a";
      };

      programs.zoxide = {
        enable = true;
        enableZshIntegration = true;
      };

      programs.zsh = {
        enable = true;
        enableCompletion = true;
        oh-my-zsh = {
          enable = true;
          plugins = [ "git" "httpie" "ripgrep" "sudo" "systemd" "tmux" ];
          theme = "af-magic";
        };
        shellAliases = {
          cat = "bat";
          nixos = "nixos-rebuild";
        };
      };

      xdg.configFile = {
        "kak-lsp/kak-lsp.toml".source = ../../cfgs/kak-lsp.toml;
        "nix/nix.conf".source = ../../cfgs/nix.conf;
        "nixpkgs/config.nix".source = ../../cfgs/nixpkgs-config.nix;
      };
    };
  };
}
