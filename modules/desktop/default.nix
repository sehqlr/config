{ pkgs, ... }: {
  fonts.fontconfig.enable = true;

  services.flatpak.enable = true;

  users.groups.vboxusers = {};
  users.users.sehqlr.extraGroups = [ "audio" "video" "vboxusers" ];

  home-manager.users.sehqlr = {
    programs.alacritty.enable = true;
    programs.firefox.enable = true;
  };

  virtualisation.virtualbox.host = {
    enable = true;
    enableExtensionPack = true;
    enableHardening = true;
  };

}
