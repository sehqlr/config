{
  networking.firewall.allowedTCPPorts = [ 80 443 ];

  security.acme.acceptTerms = true;

  services.nginx.enable = true;
  services.nginx.recommendedProxySettings = true;
  services.nginx.recommendedTlsSettings = true;

  # WEBSITES
  # samhatfield.me
  security.acme.certs."samhatfield.me".email = "hey@samhatfield.me";
  services.nginx.virtualHosts."samhatfield.me" = {
    enableACME = true;
    forceSSL = true;
    root = "/var/www/html";
  };

  services.borgbackup.jobs.www = {
    paths = "/var/www/html";
    repo = "/root/backup";
    encryption = {
      mode = "repokey";
      passCommand = "cat /root/borgbackup_passphrase";
    };
    compression = "auto,lzma";
    startAt = "daily";
  };
}
