{ config, lib, pkgs, ... }: {
  services.monica = {
    enable = true;
    hostname = "monica.samhatfield.me";
    nginx = {
        forceSSL = true;
        enableACME = true;
    };
    appKeyFile = "/var/lib/monica/monica-appkey";
    mail = {
      driver = "smtp";
      user = "hey@samhatfield.me";
      passwordFile = "/var/lib/monica/monica-fastmail";
      host = "smtp.fastmail.com";
      port = 465;
      encryption = "tls";
      from = "hey@samhatfield.me";
      fromName = "Monica";
    };
  };

  security.acme.certs."monica.samhatfield.me".email = "hey@samhatfield.me";

  services.borgbackup.jobs.monica = {
    paths = "/var/lib/monica";
    repo = "/root/backup";
    encryption = {
      mode = "repokey";
      passCommand = "cat /root/borgbackup_passphrase";
    };
    compression = "auto,lzma";
    startAt = "daily";
  };
}
