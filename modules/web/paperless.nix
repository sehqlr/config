{ config, lib, pkgs, ... }: {
  services.paperless = {
    enable = true;
    extraConfig = {
      PAPERLESS_DISABLE_LOGIN = "false";
      PAPERLESS_FORGIVING_OCR = "true";
    };
  };

  services.borgbackup.jobs.paperless = {
    paths = "/var/lib/paperless";
    repo = "/root/backup";
    encryption = {
      mode = "repokey";
      passCommand = "cat /root/borgbackup_passphrase";
    };
    compression = "auto,lzma";
    startAt = "daily";
  };

  security.acme.certs."paperless.samhatfield.me".email = "hey@samhatfield.me";
  services.nginx.virtualHosts."paperless.samhatfield.me" =
    let cfg = config.services.paperless;
    in {
      enableACME = true;
      forceSSL = true;
      locations = {
        "/" = {
          proxyPass = "http://${cfg.address}:${toString cfg.port}";
          extraConfig = ''
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
            proxy_set_header X-Real_IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
          '';
        };
      };
    };
}
