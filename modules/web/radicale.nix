{ config, pkgs, ... }: {
  services.radicale = {
    enable = true;
    package = pkgs.radicale;
    rights = {
      root = {
        user = ".+";
        collection = "";
        permissions = "R";
      };
      principal = {
        user = ".+";
        collection = "{user}";
        permissions = "RW";
      };
      calendars = {
        user = ".+";
        collection = "{user}/[^/]+";
        permissions = "rw";
      };
    };
    settings = {
      server.hosts = [ "0.0.0.0:5232" "[::]:5232" ];
      auth.type = "htpasswd";
      auth.htpasswd_filename = "/var/lib/radicale/users";
      auth.htpasswd_encryption = "bcrypt";
      storage.filesystem_folder = "/var/lib/radicale/collections";
    };
  };

  services.borgbackup.jobs.radicale = {
    paths = "/var/lib/radicale";
    repo = "/root/backup";
    encryption = {
      mode = "repokey";
      passCommand = "cat /root/borgbackup_passphrase";
    };
    compression = "auto,lzma";
    startAt = "daily";
  };

  security.acme.certs."radicale.samhatfield.me".email = "hey@samhatfield.me";
  services.nginx.virtualHosts."radicale.samhatfield.me" =
    let cfg = config.services.radicale;
    in {
      enableACME = true;
      forceSSL = true;
      locations = {
        "/" = {
            return = "301 /radicale$request_uri";
        };
        "/radicale/" = {
          proxyPass = "http://localhost:5232/";
          extraConfig = ''
            proxy_set_header X-Script-Name /radicale;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header Host $host;
            proxy_pass_header Authorization;
          '';
        };
      };
    };
}
