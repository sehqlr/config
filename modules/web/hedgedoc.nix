{ config, pkgs, ... }: {
  environment.systemPackages = [ pkgs.hedgedoc-cli ];
  services.hedgedoc = {
    enable = true;
    environmentFile = "/var/lib/hedgedoc/hedgedoc.env";
    settings = {
      allowEmailRegister = false;
      db.dialect = "postgres";
      dbURL = "postgres://hedgedocdb:\${DB_PASS}@localhost:5432/hedgedocdb";
      defaultNotePath = "/var/lib/hedgedoc/default.md";
      defaultPermission = "private";
      domain = "docs.samhatfield.me";
      protocolUseSSL = true;
      urlAddPort = false;
      sessionSecret = "\${SECRET}";
    };
  };

  services.postgresql = {
    enable = true;
    package = pkgs.postgresql_14;
    ensureDatabases = [ "hedgedocdb" ];
    ensureUsers = [{
      name = "hedgedocdb";
      ensureDBOwnership = true;
    }];
    authentication = ''
      local wiki hedgedocdb trust
    '';
  };

  services.borgbackup.jobs.hedgedoc = {
    paths = "/var/lib/hedgedoc";
    repo = "/root/backup";
    encryption = {
      mode = "repokey";
      passCommand = "cat /root/borgbackup_passphrase";
    };
    compression = "auto,lzma";
    startAt = "daily";
  };

  security.acme.certs."docs.samhatfield.me".email = "hey@samhatfield.me";
  services.nginx.virtualHosts."docs.samhatfield.me" =
    let cfg = config.services.hedgedoc;
    in {
      enableACME = true;
      forceSSL = true;
      locations = {
        "/" = {
          proxyPass = "http://localhost:${toString cfg.settings.port}";
          extraConfig = ''
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
          '';
        };
        "/socket.io/" = {
          proxyPass = "http://localhost:${toString cfg.settings.port}";
          extraConfig = ''
            proxy_set_header Host $host;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection $connection_upgrade;
          '';
        };
      };
    };
}
