{ config, lib, pkgs, ... }: {
  imports = [ ./hedgedoc.nix ./monica.nix ./paperless.nix ./radicale.nix ./www.nix ];
}
